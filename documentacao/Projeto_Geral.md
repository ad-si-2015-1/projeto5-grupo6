O jogo � uma especie de pedra-papel-tesoura incrementado. Ganhou notoriedade atravez da serie The Big Bang Theory, da Warner. O jogo funciona com dois jogadores acessando um server, que serve de mediador. O jogo e jogado em melhor de 5.

O jogo � jogado atravez de uma p�gina de internet, onde o usu�rio digita o nome e inicia o jogo escolhendo umas das 5 op��es.
Caso j� existam dois jogadores jogando, o servidor informa que est� cheio e pra tentar novamente depois.
Depois que os dois jogadores escolhem o servidor informa quem ganhou a rodada. Caso um dos dois jogadores atinja 3 pontos, o jogo � encerrado e o vencedor explicitado na p�gina. Caso um jogador saia, o jogo abre vaga para um novo jogador.