var http = require('http');
var fs = require('fs');
var socketio = require('socket.io');
url = require('url');

global.jogadorUm = criarJogador();
global.jogadorDois = criarJogador();
global.sockets = [];

var server = http.createServer(function(req, res) {
    var request = url.parse(req.url, true);
    var action = request.pathname;
    if(action.indexOf("imagens") > -1){
        var img = fs.readFileSync(__dirname + action);
        res.writeHead(200, {'Content-Type': 'image/jpg' });
        res.end(img, 'binary');
    }
    if(action.indexOf("css") > -1){
        var css = fs.readFileSync(__dirname + action);
        res.writeHead(200, {'Content-Type': 'text/css'});
        res.end(css);
    }
    res.writeHead(200, { 'Content-type': 'text/html'});
    res.end(fs.readFileSync(__dirname + '/index.html'));
}).listen(8080, function() {
    console.log('Server online em: http://localhost:8080');
});

socketio.listen(server).on('connection', function (socket) {
    socket.on('entrar', function(nome){
        if(!global.jogadorUm.ativo){
            global.jogadorUm.id = socket.id;
            global.jogadorUm.nome = nome;
            global.jogadorUm.ativo = true;
            global.sockets.push(socket);
            socket.emit('podeJogar', 'Bem vindo ao jogo ' + nome + ' você é o player 1!');
        }else if(!global.jogadorDois.ativo){
            global.jogadorDois.id = socket.id;
            global.jogadorDois.nome = nome;
            global.jogadorDois.ativo = true;
            global.sockets.push(socket);
            socket.emit('podeJogar', 'Bem vindo ao jogo ' + nome + ' você é o player 2!');
        }else{
            socket.emit('naoPodeJogar', 'Servidor ocupado, tente novamente mais tarde');
        }
        console.log(socket.id);
    });

    socket.on('escolha', function(escolha){
        if(socket.id === global.jogadorUm.id){
            global.jogadorUm.escolha = escolha;
		}
        else{
            global.jogadorDois.escolha = escolha;
		}

        if(global.jogadorUm.escolha && global.jogadorDois.escolha){
            var resultado = getResultadoRodada();
			global.sockets.forEach(function (socketJogador){
                socketJogador.emit('resultadoRodada', resultado);
				if(global.jogadorUm.pontuacao === 3 || global.jogadorDois.pontuacao === 3){
					socketJogador.emit('fimDeJogo', global.jogadorUm.pontuacao === 3 ? global.jogadorUm.nome : global.jogadorDois.nome);
				}
            });
            global.jogadorUm.escolha = '';
            global.jogadorDois.escolha = '';
			if(global.jogadorUm.pontuacao === 3 || global.jogadorDois.pontuacao === 3){
				global.jogadorUm = criarJogador();
				global.jogadorDois = criarJogador();
			}
        }
    });
	
	socket.on('disconnect', function(){
        global.sockets.forEach(function (socketJogador){
			socketJogador.emit('desistiu');
			global.jogadorUm = criarJogador();
			global.jogadorDois = criarJogador();
		});
    });
});

function criarJogador(jogador){
    return {
        'id' : 0,
        'nome' : '',
        'ativo' : false,
        'escolha' : '',
        'pontuacao' : 0
    };
}

function criarResultado(cabecalho){
    return {
        'cabecalho' : cabecalho,
        'escolhaUm' : global.jogadorUm.escolha,
        'pontosUm' : global.jogadorUm.pontuacao,
        'escolhaDois' : global.jogadorDois.escolha,
		'nomeUm' : global.jogadorUm.nome,
		'nomeDois' : global.jogadorDois.nome,
        'pontosDois' : global.jogadorDois.pontuacao
    }
}

function getResultadoRodada(){

    var nomeVencedor = '';

    if(jogadorUm.escolha === jogadorDois.escolha){
            return criarResultado("Empate!");
    }else{
        switch(jogadorUm.escolha){
            case "tesoura":
                if(jogadorDois.escolha === "papel" || jogadorDois.escolha === "lagarto"){
                    jogadorUm.pontuacao += 1;
                    nomeVencedor = jogadorUm.nome;
                }else{
                    jogadorDois.pontuacao += 1;
                    nomeVencedor = jogadorDois.nome;
                }
                break;
            case "spock":
                if(jogadorDois.escolha === "tesoura" || jogadorDois.escolha === "pedra"){
                    jogadorUm.pontuacao += 1;
                    nomeVencedor = jogadorUm.nome;
                }else{
                    jogadorDois.pontuacao += 1;
                    nomeVencedor = jogadorDois.nome;
                }
                break;
            case "pedra":
                if(jogadorDois.escolha === "tesoura" || jogadorDois.escolha === "lagarto"){
                    jogadorUm.pontuacao += 1;
                    nomeVencedor = jogadorUm.nome;
                }else{
                    jogadorDois.pontuacao += 1;
                    nomeVencedor = jogadorDois.nome;
                }
                break;
            case "lagarto":
                if(jogadorDois.escolha === "papel" || jogadorDois.escolha === "spock"){
                    jogadorUm.pontuacao += 1;
                    nomeVencedor = jogadorUm.nome;
                }else{
                    jogadorDois.pontuacao += 1;
                    nomeVencedor = jogadorDois.nome;
                }
                break;
            case "papel":
                if(jogadorDois.escolha === "pedra" || jogadorDois.escolha === "spock"){
                    jogadorUm.pontuacao += 1;
                    nomeVencedor = jogadorUm.nome;
                }else{
                    jogadorDois.pontuacao += 1;
                    nomeVencedor = jogadorDois.nome;
                }
                break;
        }
        return criarResultado(nomeVencedor);
    }
}
